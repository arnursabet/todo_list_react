import React, { Component } from 'react';

class Item extends Component {

  onCompleteHandle(event) {
    event.target.parentNode.classList.add('done');
  }

  render() {
    console.log(this);
    return (
      <li>
        {this.props.title}
        <button onClick={this.props.delete} className="float-right"><i className="fa fa-trash"></i></button>
        <button onClick={this.props.edit} className="float-right"><i className="fa fa-edit"></i></button>
        <button onClick={this.onCompleteHandle.bind(this)} className="float-right"><i className="fa fa-check-circle"></i></button>
      </li>
    );
  }
}

export default Item;
