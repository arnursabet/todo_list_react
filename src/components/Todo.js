import React, { Component } from 'react';
import SimpleStorage from "react-simple-storage";

class Todo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      id: null,
      mockData: [ //ARRAY mockData OF 4 INITIAL LIST ITEMS DICTIONARIES
        {
          id: '1',
          title: 'Build React Todo List',
          done: false,
          date: new Date()
        }, 
        {
          id: '2',
          title: 'Learn React',
          done: false,
          date: new Date()
        }, 
        {
          id: '3',
          title: 'Study Udemy Web Development',
          done: false,
          date: new Date()
        }, 
        {
          id: '4',
          title: 'Make HTML Ad banner',
          done: false,
          date: new Date()
        }
      ]
    }
  }


// SUBMIT

  onSubmitHandle(event) {
    event.preventDefault();

    this.setState({
      mockData: [...this.state.mockData, 
        {
          id: Date.now(), // GENEREATES UNIQUE IDs USING THE CURRENT DATE-TIME
          title: event.target.item.value,
          done: false,
          date: new Date()
        }
      ]
    });

    event.target.item.value = ''; // SETS THE FORM TEXT TO AN EMPTY STRING AFTER SUBMISSION
  }

  // DELETE

  onDeleteHandle() {
    let id = arguments[0];

    this.setState({
      mockData: this.state.mockData.filter(item => {
          if (item.id !== id) {
            return item;
          }
        })
    });
  }


  // EDIT & UPDATE

  onEditHandle(event) {
    this.setState({
      edit: true,
      id: arguments[0],
      title: arguments[1]
    });
  }

  onUpdateHandle = (event) => {
    event.preventDefault();

    this.setState({
      mockData: this.state.mockData.map(item => {
        if (item.id === this.state.id) {
          item['title'] = event.target.updatedItem.value;
          return item;
        }
        return item;
      })
    });

    this.setState({
      edit: false
    });
  }

  // COMPLETE

  onCompleteHandle = (id) => () => {
    this.setState({
      mockData: this.state.mockData.map(item => {
        if (item.id === id) {
          item['done'] = true;
          return item;
        }
        return item;
      })
    });
  }

 
  // RENDER EDIT FORM

  renderEditForm() {
    if (this.state.edit) {
      return <form onSubmit={this.onUpdateHandle} className="form-style">
        <input type="text" name="updatedItem" className="item" defaultValue={this.state.title} />
        <button className="btn btn-outline-primary update-add-item">Update</button>
      </form>
    }
  }

  render() {
    return (
      <div className="container">
        <SimpleStorage parent={this} />
        {this.renderEditForm() /* CHECK ON EDIT ITEM FORM HERE  */} 

        <form onSubmit={this.onSubmitHandle.bind(this)} className="form-style"> {/* ADD ITEM FORM */}
          <input type="text" name="item" className="item" />
          <button className="btn btn-outline-success btn-add-item">Add</button>
        </form>

        <ul className="list-group"> {/* LIST OF ITEMS */}
          {this.state.mockData.map(item => (
            <li key={item.id} className={ item.done ? 'done list-group-item' : 'hidden list-group-item' }>  
              {item.title}
              <div className="buttons-container">
                {/* DELETE BUTTON */}
                <button onClick={this.onDeleteHandle.bind(this, item.id)}><i className="fa fa-trash"></i></button>
                {/* EDIT BUTTON */}
                <button onClick={this.onEditHandle.bind(this, item.id, item.title)}><i className="fa fa-pencil"></i></button>
                {/* COMPLETE BUTTON */}
                <button onClick={this.onCompleteHandle(item.id)}><i className="fa fa-check-circle"></i></button>
              </div>
            </li> 
          ))}
        </ul>
      </div>
    );
  }
}

export default Todo;
