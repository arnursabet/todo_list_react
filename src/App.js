import React, { Component } from 'react';
import './App.css';

import Todo from './components/Todo';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      show: false
    };
  }

  render() {
    return (
      <div className="App">
        <header>
          <nav class="navbar navbar-light bg-light">  {/* NAV BAR */}
            <a class="navbar-brand" href="#!">Todo List</a>
          </nav>
        </header>
        <Todo />
      </div>
    );
  }
}

export default App;